class Player < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  #before_validation :create_login_and_password
  devise :database_authenticatable,
         #:registerable,
         #:recoverable,
         :rememberable,
         :trackable,:validatable
  has_and_belongs_to_many :games

  has_many :killers_cards, class_name: "Card", foreign_key: :owner_id, dependent: :destroy
  has_one :victims_card, class_name: "Card", foreign_key: :victim_id, dependent: :destroy


  has_many :victims, through: :killers_cards
  has_one :killer, through: :victims_card

  has_many :source_events, class_name: 'Event', foreign_key: 'source_id'
  has_many :target_events, class_name: 'Event', foreign_key: 'target_id'
  has_many :new_target_events, class_name: 'Event', foreign_key: 'new_target_id'

  has_attached_file :avatar, styles: {medium: ['450x300', :jpg], thumb: ['150x100#', :jpg], small: ['32x32#', :jpg]},
                    url: "/system/avatars/:hash.:extension", hash_secret: 'killerfiztehbymorodeer',
                    default_url: "/system/avatars/:style/missing.jpg"

  has_many :requests

  belongs_to :target, class_name: 'Player'

  before_create do |player|
    player.build_victims_card owner: player
  end

  def die
		self.update_attribute(:dead,true)
  end

	def kill(player,password,method)
		if player.killing_word == password && self.target.id == player.id
			player.die
			player.victims_card.kill if player.victims_card
			player.killers_cards.each do |card|
				Event.create(event_type:'card_transfer',source:self,target:player,card:card,method:method)
			end
			killers_cards << player.killers_cards
			Event.create(event_type:'murder',source:self,target:player,method:method, new_target: self.target, card_count: self.killers_cards.count)
		end
	end

	def full_name
		"#{name} #{surname}"
	end

  def email_required?
		false
  end

  def email_changed?
		false
  end

  def update_killing_word
	  self.killing_word = SecureRandom.base64(6)
	  self.save
  end

  def update_login_and_password
	  self.login = (0..8).map{ ('a'..'z').to_a[rand(26)]}.join
	  self.password = (0..8).map{ ('a'..'z').to_a[rand(26)]}.join
	  self.password_confirmation = self.password
	  self.temp_password = self.password
		self.save
  end

  def avatar_thumb
    avatar.url(:thumb)
  end

  def killers_cards_count
    killers_cards.count
  end

  def current_game
    games.where("starts_at < ? AND ends_at > ?", Time.now, Time.now).first
  end

  def upcoming_game
    games.where("starts_at > ?", Time.now).first
  end

  def status
    dead ? "Убит" : "Жив"
  end
	#private
	#	def create_login_and_password
   #   if self.login == ""
  #
   #     self.login = (0..8).map{ ('a'..'z').to_a[rand(26)]}.join
   #     self.password = (0..8).map{ ('a'..'z').to_a[rand(26)]}.join
   #     self.password_confirmation = self.password
   #     self.temp_password = self.password
  	#		self.killing_word = SecureRandom.base64(6)
   #   end
  #
   #   #self.save
	#	end

  def to_s
    full_name
  end

end
