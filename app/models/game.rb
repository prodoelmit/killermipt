class Game < ActiveRecord::Base
  attr_accessor :password_confirmation #, :start_time, :start_date, :end_time, :end_date
  has_and_belongs_to_many :admins
  has_and_belongs_to_many :players
  has_many :requests
  has_many :events

  has_attached_file :logo, styles: {medium: ['300x300', :jpg]},
                    url: "/system/logo/:hash.:extension", hash_secret: 'killerfiztehbymorodeer',
                    default_url: "/system/logo/:style/missing.jpg"

  validates_attachment_content_type :logo, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  validates :name, :starts_at, :ends_at, presence: true

  def started?
    Time.now > starts_at
  end

  def ended?
    closed || Time.now > ends_at
  end

  def status
    if ended? then 'Завершена'
    elsif started? then 'Начата'
    else 'Ожидает начала'
    end
  end
end
