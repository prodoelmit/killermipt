class Request < ActiveRecord::Base
  belongs_to :player
  belongs_to :game

  validates :player, :game, presence: true

  before_create do |request|
    request.status ||= 'pending'
  end

  def reject
    update_attributes(status: 'rejected')
  end

  def approve
    update_attributes(status: 'approved')
  end

  def pending?
    status == 'pending'
  end

  def rejected?
    status == 'rejected'
  end

  def approved?
    status == 'approved'
  end
end
