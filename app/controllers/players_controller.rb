class PlayersController < ApplicationController
	#before_filter :authenticate_admin!, only: [:new, :create, :destroy, :show, :index, :admin_kill, :edit,
  # :print_list, :set_target]
  before_filter :authenticate_admin!, only: [:index, :edit]
	before_filter :authenticate_player!, except: [:new, :create]
  #before_filter :stop_game, except: [:show, :index]

	def new
		@player = Player.new
		@players = Player.all
	end

	def create
		@player = Player.create(params.require(:player).permit(:name,:surname,:email,:password, :password_confirmation, :vklink))
		if @player.save
      redirect_to root_path
		else
			render 'new'
		end
	end

	def destroy
		@player = Player.find_by_id(params[:id])
		@player.destroy
    redirect_to root_path
	end

	def index
		#@players = Player.paginate(page:params[:page])
		@players = Player.all
	end

	def edit_info
		@player = current_player
	end

	def edit
		@player = Player.find_by_id(params[:id])
	end

	def update    
		if admin_signed_in?
			@player = Player.find_by_id(params[:player][:id])
			if @player.update_attributes(player_params)
				redirect_to edit_player_path(@player), success: 'Параметры обновлены'
			else
				render 'edit'
			end
		elsif player_signed_in? and current_player.id.to_i == params[:player][:id].to_i
			@player = Player.find_by_id(params[:player][:id])

      if @player.current_game && @player.current_game.avatar_changeable
        permitparams = player_params_with_avatar
      else
        permitparams = player_params
      end

			if @player.update_attributes(permitparams)
				redirect_to new_player_session_path, success: 'Параметры обновлены'
			else
				render 'edit_info'
			end
		else
			redirect_to root_path, alert: 'У вас на это нет прав'
		end


	end

	def kill
		@target = Player.find_by_id(  params[:target][:id])
		@player = current_player
		if @player.target.id == @target.id
			puts 'starting killing'
			if @player.kill(@target,params[:target][:killing_word],'web')
				redirect_to root_path, success: 'Трупов всё больше, спасибо вам! )'
			else
				redirect_to root_path, alert: 'Промахнулись с жертвой или паролем. Брутфорс запрещен'
			end
		else
			redirect_to root_path, alert: 'У вас нет на это прав'
		end
	end

	def admin_kill
		@target = Player.find_by_id(params[:target][:id])
		@player = Player.find_by_id(params[:player][:id])
		if @player.target.id == @target.id
			puts 'starting killing'
			if @player.kill(@target,params[:target][:killing_word],'admin')
				redirect_to player_path(@player), success: 'Ок, убили.'
			else
				redirect_to player_path(@player), alert: 'С паролем не промахивайтесь =)'
			end
		end
	end

	def show
		@player = Player.find_by_id(params[:id])
		@target = @player.target
		@events = Event.where(event_type:'murder',source_id:@player.id)
		@killers_card = Card.where(owner: @player, victim: @target)
	end

	def set_target
		@player = Player.find_by_id(params[:id])
		@target = Player.find_by_id(params[:player][:id])
			Card.create(victim: @target, owner: @player)
		redirect_to @player
  end

  def revive
    @player = Player.find_by_id(params[:id])
    if @player.update_attributes(dead:nil)
      redirect_to @player
    else
      render 'show'
    end
  end

	private
		def is_current_player
			authenticate_player!
			unless params[:id].to_i == current_player.id.to_i
				redirect_to root_path, error: 'У вас на это нет прав'
			end
		end

    def stop_game
      redirect_to root_path
    end

    def player_params
      p_params = params.require(:player).permit(:name, :surname, :login, :vklink, :password, :password_confirmation)
      if p_params[:password].blank?
        p_params.delete("password")
        p_params.delete("password_confirmation")
      end
      p_params
    end

    def player_params_with_avatar
      p_params = params.require(:player).permit(:name, :surname, :login, :vklink, :password, :password_confirmation, :avatar)
      if p_params[:password].blank?
        p_params.delete("password")
        p_params.delete("password_confirmation")
      end
      p_params
    end
end
