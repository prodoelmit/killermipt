class CardsController < ApplicationController
	before_filter :authenticate_admin!
	def destroy
		@card = Card.find_by_id(params[:id])
		@player = @card.owner
		@card.destroy
		redirect_to @player
	end
end
