class AdminsController < ApplicationController
	before_filter :authenticate_admin!, except: [:new, :create]

  def new
    @admin = Admin.new
  end

  def create
    @admin = Admin.create(params.require(:admin).permit(:name,:email,:password,:password_confirmation))
    if @admin.save
      redirect_to games_path
    else
      render 'new'
    end
  end

	def edit_info
		@admin = current_admin
	end

	def update
		@admin = Admin.find_by_id(params[:admin][:id])
		if @admin.update_attributes(params.require(:admin).permit([:email,:password,:password_confirmation,:name]))
			redirect_to root_path, success: 'Параметры обновлены'
		else
			render 'edit_info', alert: 'Couldn\'t update your profile settings. Please check fields for errors'
		end
	end
end
