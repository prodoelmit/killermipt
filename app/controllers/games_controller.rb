class GamesController < ApplicationController
  before_filter :authenticate_player!


  def new
    @game = Game.new
  end


  def index
    @games = Game.all
    @requested_games = current_player.requests.map &:game
  end
end
