class RequestController < ApplicationController
  before_filter :authenticate_player!, only: [:new, :create]

  def new
    @game = Game.find(params[:game_id])
    @request = Request.new game: @game
  end

  def create
    @request = Request.new(request_params)
    @request.player = current_player
    if @request.save
      redirect_to current_player
    else
      render "new"
    end
  end

private
  def request_params
    params.require(:request).permit(:game_id, :answer1, :answer2, :answer3, :answer4)
  end
end
