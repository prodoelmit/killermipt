class Admin::RequestsController < ApplicationController
  before_action :set_request, only: [:edit, :update]
  before_filter :authenticate_admin!

  # GET /admin/requests/1/edit
  def edit
    @player = @request.player
  end

  # PATCH/PUT /admin/requests/1
  # PATCH/PUT /admin/requests/1.json
  def update
    respond_to do |format|
      if @request.update(request_params)
        format.html { redirect_to admin_game_path(@request.game)}
        format.json { head :no_content }

        if @request.approved?
          @request.player.games = [@request.game]
          @request.player.dead = false
          @request.player.banned = false
          @request.player.ban_reason = ''
          @request.save
        end
      else
        format.html { render action: 'edit' }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_request
      @request = Request.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def request_params
      params.require(:request).permit(:status)
    end
end
