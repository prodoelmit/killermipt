class Admin::PlayersController < ApplicationController
  before_action :set_player, only: [:show, :edit, :update, :kill, :alive]
  before_filter :authenticate_admin!

  def show
    respond_with(@player)
  end

  def edit
    @possible_targets = game.players.where(dead: false).where.not(id: @player.id)
  end

  def update
    @player.update(player_params)
    redirect_to admin_game_path(game)
  end

  def kill
    @player.update dead: true
    redirect_to admin_game_path(game)
  end

  def alive
    @player.update dead: false
    redirect_to admin_game_path(game)
  end

  private
    def set_player
      @player = Player.find(params[:id])
    end

    def player_params
      params.require(:player).permit(:dead, :killing_word, :banned, :ban_reason, :target_id)
    end

    def game
      @player.current_game || @player.upcoming_game
    end
end
