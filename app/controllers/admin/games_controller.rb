class Admin::GamesController < ApplicationController
  before_filter :authenticate_admin!
  def index
    @player = current_admin
    @games = @player.games
  end

  def new
    @game = Game.new
    @game.starts_at = Time.now
    @game.ends_at = Time.now + 7.days
  end

  def create
    @game = Game.create(game_params)
    if @game.password == @game.password_confirmation
      if @game.save
        @game.admins << current_admin
        redirect_to admin_game_path(@game)
      end
    end

  end

  def show
    @admin = current_admin
    @game = Game.find_by_id(params[:id])
    @requests = @game.requests.where status: 'pending'
    @players = @game.players
  end

  def edit
    @game = Game.find_by_id(params[:id])
  end

  def update
    @game = Game.find_by_id(params[:id])

    if params[:password] == params[:password_confirmation]
      if @game.update_attributes(game_params)
        redirect_to admin_game_path(@game)
      end
    else
      render 'edit'
    end

  end

  def print_list
    @game = Game.find(params[:game_id])
    @players = @game.players
    respond_to do |format|
      format.html
      format.pdf do
            pdf = PlayersPdf.new(@players, @game)
        send_data pdf.render, filename: "playerspdf.pdf",type:"application/pdf", disposition: "inline"
      end
    end
  end

  private
    def game_params
      params.require(:game).permit(:name,:starts_at,:ends_at,:password, :password_confirmation, :closed,
        :question1, :question2, :question3, :question4, :logo)
    end

end
