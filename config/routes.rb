KillerFalt::Application.routes.draw do
  get "request/new/:game_id", to: 'request#new'
  post "request/create", to: 'request#create'

  namespace :admin do
    resources :players, only: [:show, :edit, :update] do
      member do
         get 'kill'
         get 'alive'
       end
    end
    resources :requests, only: [:edit, :update]
  end

  devise_for :admins
  devise_for :players

	resources :players do
		member do
			post 'kill'
			post 'admin_kill'
			post 'set_target'
      post 'revive'
		end
  end


  resources :games
  namespace :admin do
    resources :games
  end
  get '/edit_info', to: 'players#edit_info'
  get '/edit_admin_info', to: 'admins#edit_info'
  get '/print_players/:game_id', to: 'admin/games#print_list'
	resources :admins
  resources :events
  resources :cards

  root to: 'static_pages#home'
  get '/static_pages/home', to: 'static_pages#home'
	get '/rules', to: 'static_pages#rules'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
