require 'test_helper'

class Admin::PlayersControllerTest < ActionController::TestCase
  setup do
    @admin_player = admin_players(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_players)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_player" do
    assert_difference('Admin::Player.count') do
      post :create, admin_player: {  }
    end

    assert_redirected_to admin_player_path(assigns(:admin_player))
  end

  test "should show admin_player" do
    get :show, id: @admin_player
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_player
    assert_response :success
  end

  test "should update admin_player" do
    patch :update, id: @admin_player, admin_player: {  }
    assert_redirected_to admin_player_path(assigns(:admin_player))
  end

  test "should destroy admin_player" do
    assert_difference('Admin::Player.count', -1) do
      delete :destroy, id: @admin_player
    end

    assert_redirected_to admin_players_path
  end
end
