require 'test_helper'

class Admin::RequestsControllerTest < ActionController::TestCase
  setup do
    @admin_request = admin_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_request" do
    assert_difference('Admin::Request.count') do
      post :create, admin_request: {  }
    end

    assert_redirected_to admin_request_path(assigns(:admin_request))
  end

  test "should show admin_request" do
    get :show, id: @admin_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_request
    assert_response :success
  end

  test "should update admin_request" do
    patch :update, id: @admin_request, admin_request: {  }
    assert_redirected_to admin_request_path(assigns(:admin_request))
  end

  test "should destroy admin_request" do
    assert_difference('Admin::Request.count', -1) do
      delete :destroy, id: @admin_request
    end

    assert_redirected_to admin_requests_path
  end
end
