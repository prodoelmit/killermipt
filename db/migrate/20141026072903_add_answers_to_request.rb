class AddAnswersToRequest < ActiveRecord::Migration
  def change
    add_column :requests, :answer1, :string
    add_column :requests, :answer2, :string
    add_column :requests, :answer3, :string
    add_column :requests, :answer4, :string
  end
end
