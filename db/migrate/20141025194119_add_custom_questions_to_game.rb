class AddCustomQuestionsToGame < ActiveRecord::Migration
  def change
    add_column :games, :question1, :string
    add_column :games, :question2, :string
    add_column :games, :question3, :string
    add_column :games, :question4, :string
  end
end
