class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|

      t.timestamps
      t.belongs_to :player
      t.belongs_to :game
      t.string :status #pending, approved, rejected
    end
  end
end
