class AddEmailToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :email, :string
    remove_column :admins, :surname
    remove_column :admins, :login
  end
end
