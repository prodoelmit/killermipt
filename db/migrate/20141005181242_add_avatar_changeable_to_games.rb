class AddAvatarChangeableToGames < ActiveRecord::Migration
  def change
    add_column :games, :avatar_changeable, :boolean
  end
end
