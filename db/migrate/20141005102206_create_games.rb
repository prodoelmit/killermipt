class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :name
      t.datetime :starts_at
      t.datetime :ends_at
      t.string :additionalfields
      t.boolean :with_photo
      t.text :rules
      t.timestamps
    end

    create_table :admins_games, id: false do |t|
      t.belongs_to :game
      t.belongs_to :admin
    end

    add_column :events, :game_id, :integer

    create_table :games_players, id: false do |t|
      t.belongs_to :game
      t.belongs_to :player
    end
  end

end
