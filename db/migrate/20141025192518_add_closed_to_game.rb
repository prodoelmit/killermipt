class AddClosedToGame < ActiveRecord::Migration
  def change
    add_column :games, :closed, :boolean
  end
end
