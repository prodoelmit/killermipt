class AddAttachmentAvatarToPlayers < ActiveRecord::Migration
  def change
    change_table :players do |t|
      t.attachment :avatar
    end
  end

end
