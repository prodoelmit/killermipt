# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141026110937) do

  create_table "admins", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "email"
  end

  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true

  create_table "admins_games", id: false, force: true do |t|
    t.integer "game_id"
    t.integer "admin_id"
  end

  create_table "cards", force: true do |t|
    t.integer  "victim_id",                  null: false
    t.integer  "owner_id",                   null: false
    t.integer  "killer_id"
    t.boolean  "killed",     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cards", ["killer_id"], name: "index_cards_on_killer_id", unique: true
  add_index "cards", ["victim_id"], name: "index_cards_on_victim_id", unique: true

  create_table "events", force: true do |t|
    t.string   "event_type"
    t.integer  "source_id"
    t.integer  "target_id"
    t.integer  "card_id"
    t.integer  "card_count"
    t.integer  "new_target_id"
    t.string   "method"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "game_id"
  end

  create_table "games", force: true do |t|
    t.string   "name"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.string   "additionalfields"
    t.boolean  "with_photo"
    t.text     "rules"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.boolean  "avatar_changeable"
    t.string   "password"
    t.boolean  "closed"
    t.string   "question1"
    t.string   "question2"
    t.string   "question3"
    t.string   "question4"
  end

  create_table "games_players", id: false, force: true do |t|
    t.integer "game_id"
    t.integer "player_id"
  end

  create_table "players", force: true do |t|
    t.string   "name"
    t.string   "surname"
    t.boolean  "dead"
    t.string   "killing_word"
    t.integer  "bf_count"
    t.boolean  "banned"
    t.string   "ban_reason"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "temp_password"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.boolean  "passwordchanged"
    t.string   "vklink"
    t.integer  "target_id"
  end

  add_index "players", ["email"], name: "index_players_on_email", unique: true
  add_index "players", ["reset_password_token"], name: "index_players_on_reset_password_token", unique: true

  create_table "requests", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "player_id"
    t.integer  "game_id"
    t.string   "status"
    t.string   "answer1"
    t.string   "answer2"
    t.string   "answer3"
    t.string   "answer4"
  end

end
